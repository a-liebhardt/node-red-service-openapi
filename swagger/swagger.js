const DEFAULT_TEMPLATE = {
  swagger: '2.0',
  info: {
    title: 'My Node-RED API',
    version: '0.0.1'
  },
  basePath: null
};

module.exports = function (RED) {
  'use strict';

  const path = require('path');

  const defaultResponse = {
    default: {
      description: ''
    }
  };
  const convToSwaggerPath = x => `/{${x.substring(2)}}`;
  const trimAll = ary => ary.map(x => x.trim());
  const csvStrToArray = csvStr => csvStr ? trimAll(csvStr.split(',')) : [];
  const ensureLeadingSlash = url => (url.startsWith('/') ? url : '/' + url);
  const stripTerminalSlash = url => url.length > 1 && url.endsWith('/') ? url.slice(0, -1) : url;
  const capitalize = str => str.charAt(0).toUpperCase() + str.slice(1);
  const prepareParameters = (parameters) => {
    const result = {
      parameters: [],
      requestBody: {},
      schemas: {}
    };

    for (let parameter of parameters) {
      if ('body' === parameter.in) {
        if ('' === parameter.swaggerName) continue;
        if ('object' !== parameter.schema.type) continue;
        if (!parameter.schema.properties) continue;

        let properties = parameter.schema.properties;
        for (let i = 0; i < 10; i++) {
          if ('object' === typeof properties) break;
          properties = JSON.parse(properties);
        }
        if ('object' !== typeof properties) continue;

        // Overwrite request body
        result.requestBody = {
          content: {
            'application/json': {
              schema: {
                '$ref': `#/components/schemas/${capitalize(parameter.swaggerName)}`
              }
            }
          }
        }

        // Add schema to schemas
        result.schemas[capitalize(parameter.swaggerName)] = {
          type: 'object',
          properties: properties
        };

      } else {
        result.parameters.push(parameter);

      }
    }
    return result;

  };
  const prepareResponse = (responses) => {
    if (false === !!Object.keys(responses).length) {
      return defaultResponse;
    }

    const result = {
      parameters: {},
      schemas: {}
    };

    for (let responseKey in responses) {
      const response = responses[responseKey];
      result.parameters[responseKey] = response;

      if (response.schema) {
        for (let swaggerName in response.schema.properties) {
          const properties = response.schema.properties[swaggerName].schema.properties;
          const preparedParams = prepareParameters([{
            in: 'body',
            swaggerName: swaggerName,
            schema: {
              type: 'object',
              properties: properties
            }
          }]);
          result.schemas = {
            ...result.schemas,
            ...preparedParams.schemas
          };
          result.parameters[responseKey] = {
            ...response,
            ...preparedParams.requestBody
          };
          delete result.parameters[responseKey].schema;
          break;

        }
      }
    }
    return result;

  };
  const regexColons = /\/:\w*/g;

  RED.httpNode.get('/http-api/swagger.json', (req, res) => {
    const {
      httpNodeRoot,
      swagger: {
        template: resp = { ...DEFAULT_TEMPLATE }
      } = {}
    } = RED.settings;
    const { basePath = httpNodeRoot } = resp;
    resp.basePath = stripTerminalSlash(basePath);
    resp.paths = {};
    resp.components = {
      schemas: {}
    };

    RED.nodes.eachNode(node => {
      const { name, type, method, swaggerDoc, url } = node;

      if (['http in', 'socketio-receive', 'socketio-reply'].indexOf(type) > -1) {
        const swagger = RED.nodes.getNode(swaggerDoc);
        const {
          summary,
          description = '',
          tags,
          consumes,
          produces,
          deprecated,
          parameters = [],
          responses = {}
        } = swagger || {};

        const aryTags = csvStrToArray(tags);
        const aryConsumes = csvStrToArray(consumes);
        const aryProduces = csvStrToArray(produces);
        const preparedParams = prepareParameters(parameters);
        const preparedResponses = prepareResponse(responses);
        const preparedMethod = method ? method : 'options';
        const socketId = node.socketid && node.socketid.length ? node.socketid : null;
        const nodeUrl = url ? url.replace(regexColons, convToSwaggerPath) : '';
        const endPoint = socketId ? socketId : ensureLeadingSlash(nodeUrl);

        // Skip inpropper socket configurations
        if ('' === endPoint || '/' === endPoint) return;
        if (!preparedParams) return;
        // if (!preparedResponses) return;

        const config = {
          summary: summary || preparedMethod + ' ' + endPoint,
          description,
          tags: [...['HTTP'], ...aryTags],
          consumes: aryConsumes,
          produces: aryProduces,
          deprecated,
          parameters: preparedParams.parameters,
          requestBody: null,
          responses: preparedResponses.parameters
        };

        if (preparedParams.requestBody && preparedParams.requestBody.content) {
          config.requestBody = preparedParams.requestBody;
        }

        if ('http in' !== type) {
          if ('socketio-receive' === type) {
            config.tags = [ ...['SOCKET (Client/Server)'], ...aryTags];
          } else {
            config.tags = [ ...['SOCKET (Server/Client)'], ...aryTags];
          }
          config.consumes = ['application/json'],
          config.produces = ['application/json'],
          config.parameters = [
            {
              in: 'query',
              name: `${socketId}`,
              schema: {
                type: 'string'
              },
              required: true,
              description: 'The socket event name'
            }
          ];

          // Static schema overwrite test
          // resp.components.schemas.User = {
          //   type: 'object',
          //   properties: {
          //     id: {
          //       type: 'integer'
          //     },
          //     name: {
          //       type: 'string'
          //     }
          //   }
          // };

        }

        if (!resp.paths[endPoint]) {
          resp.paths[endPoint] = {};
        }
        resp.paths[endPoint][preparedMethod] = config;
        resp.components.schemas = { ...resp.components.schemas, ...preparedParams.schemas, ...preparedResponses.schemas };

      }

    });

    res.json(resp);

  });

  function SwaggerDoc(n) {
    RED.nodes.createNode(this, n);
    this.summary = n.summary;
    this.description = n.description;
    this.tags = n.tags;
    this.consumes = n.consumes;
    this.produces = n.produces;
    this.parameters = n.parameters;
    this.responses = n.responses;
    this.deprecated = n.deprecated;
  }

  RED.nodes.registerType('swagger-doc', SwaggerDoc);

  function sendFile(res, filename) {
    // Use the right function depending on Express 3.x/4.x
    if (res.sendFile) {
      res.sendFile(filename);

    } else {
      res.sendfile(filename);

    }
  }

  RED.httpAdmin.get('/swagger-ui/reqs/i18next.min.js', (req, res) => {
    const basePath = require.resolve('i18next-client').replace(/[\\/]i18next.js$/, '');
    const filename = path.join(basePath, 'i18next.min.js');
    sendFile(res, filename);
  });

  RED.httpAdmin.get('/swagger-ui/reqs/*', (req, res) => {
    const basePath = require.resolve('swagger-ui').replace(/[\\/]swagger-ui.js$/, '');
    const filename = path.join(basePath, req.params[0]);
    sendFile(res, filename);
  });

  RED.httpAdmin.get('/swagger-ui/nls/*', (req, res) => {
    const filename = path.join(__dirname, 'locales', req.params[0]);
    sendFile(res, filename);
  });

  RED.httpAdmin.get('/swagger-ui/*', (req, res) => {
    const filename = path.join(__dirname, 'swagger-ui', req.params[0]);
    sendFile(res, filename);
  });
};
